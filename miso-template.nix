{ mkDerivation, base, miso, stdenv }:
mkDerivation {
  pname = "miso-template";
  version = "0.1.0.0";
  src = ./.;
  isLibrary = false;
  isExecutable = true;
  executableHaskellDepends = [ base miso ];
  description = "Miso template for cloning";
  license = stdenv.lib.licenses.bsd3;
}
